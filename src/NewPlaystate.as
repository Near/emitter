package
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import net.pixelpracht.tmx.*;
	
	import org.flixel.*;
	
	public class NewPlaystate extends FlxState
	{
		[Embed(source="assets/tileset.png")] private var ImgTiles:Class;
		
		protected var _fps:FlxText;
		
		override public function create():void
		{
			_fps = new FlxText(0, 0, 10, "");
			loadTmxFile();
		}
		
		override public function update():void
		{
			_fps.text = FlxU.floor(1/FlxG.elapsed).toString()+"fps";
			super.update();
			if(FlxG.keys.justReleased("ENTER"))
				FlxG.switchState(new NewPlaystate());
			FlxG.collide();
		}
		
		private function loadTmxFile():void
		{
			var loader:URLLoader = new URLLoader(); 
			loader.addEventListener(Event.COMPLETE, onTmxLoaded); 
			loader.load(new URLRequest('assets/1.tmx')); 
		}
		
		private function onTmxLoaded(e:Event):void
		{
			var xml:XML = new XML(e.target.data);
			var tmx:TmxMap = new TmxMap(xml);
			loadStateFromTmx(tmx);
		}
		
		private function loadStateFromTmx(tmx:TmxMap):void
		{
			//create the flixel implementation of the objects specified in the ObjectGroup 'objects'
			//var group:TmxObjectGroup = tmx.getObjectGroup('objects');
			//for each(var object:TmxObject in group.objects)
			//	spawnObject(object)
			
			//Basic level structure
			var t:FlxTilemap = new FlxTilemap();
			//generate a CSV from the layer 'map' with all the tiles from the TileSet 'tiles'
			var mapCsv:String = tmx.getLayer('Ground').toCsv(tmx.getTileSet('haha'));
			t.loadMap(mapCsv,ImgTiles, 16, 16);
			t.follow();
			add(t);
			
			//Instructions and stuff
			_fps = new FlxText(FlxG.width-60,0,40).setFormat(null,8,0x778ea1,"right",0x233e58);
			_fps.scrollFactor.x = _fps.scrollFactor.y = 0;
			add(_fps);
		}
		
		
		/*private function spawnObject(obj:TmxObject):void
		{
			//Add game objects based on the 'type' property
			switch(obj.type)
			{
				case "elevator":
					add(new Elevator(obj.x, obj.y, obj.height));
					return;
				case "pusher":
					add(new Pusher(obj.x, obj.y, obj.width));
					return;
				case "player":
					add(new Player(obj.x,obj.y));
					return;
				case "crate":
					add(new Crate(obj.x,obj.y));
					return;
			}
		
			//This is the thing that spews nuts and bolts
			if(obj.type == "dispenser")
			{
				var dispenser:FlxEmitter = new FlxEmitter(obj.x,obj.y);
				dispenser.setSize(obj.width,obj.height);
				dispenser.setXSpeed(obj.custom['minvx'],obj.custom['maxvx']);
				dispenser.setYSpeed(obj.custom['minvy'],obj.custom['maxvy']);
				dispenser.createSprites(ImgGibs,120,16,true,0.8);
				dispenser.start(false,obj.custom['quantity']);
				add(dispenser);
			}
		}*/
		
		

	}
}