package  
{
	import org.flixel.*;
	
	public class Map extends FlxTilemap
	{
		private const numTiles:uint = 200;
		private var levels:FlxGroup = new FlxGroup();
		public function Map():void
		{
			levels = new FlxGroup();
			var tiles:FlxTileblock;
			/* add the around walls */
			tiles = new FlxTileblock(0, 0, 8, FlxG.width);	// left side
			tiles.makeGraphic(8, FlxG.height, 0xffff11ff);
			levels.add(tiles);
			tiles = new FlxTileblock(8, 0, FlxG.width - 16, 8);	// top side
			tiles.makeGraphic(FlxG.width - 16, 8, 0xffff11ff);
			levels.add(tiles);
			tiles = new FlxTileblock(FlxG.width - 8, 0, 8, FlxG.height);	// right side
			tiles.makeGraphic(8, FlxG.height, 0xffff11ff);
			levels.add(tiles);
			tiles = new FlxTileblock(8, FlxG.height - 8, FlxG.width - 16, 8);	// bottom side
			tiles.makeGraphic(FlxG.width - 16, 8, 0xffff11ff);
			levels.add(tiles);
			
			for (var i:uint = 0; i < numTiles; i ++) {
				var x:uint = Math.random() * (FlxG.width - 16) / 8;
				var y:uint = Math.random() * (FlxG.height -16) / 8;
				tiles = new FlxTileblock((x+1) * 8, (y+1) * 8, 8, 8);
				tiles.makeGraphic(8, 8);
				levels.add(tiles);
			}
			FlxG.state.add(levels);
		}
		public function getTiles():FlxGroup {
			return levels;
		}
	}

}