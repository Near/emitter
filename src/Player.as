package {
	import org.flixel.*;
	
	public class Player extends FlxSprite {
		protected static const RunSpeed:uint = 80;
		protected static const GravityAcceleratioin:uint = 420;
		protected static const JumpAcceleration:uint = 200;
		protected var sizeX:uint = 8;
		protected var sizeY:uint = 8;
		private var index:uint;
		private var partner:Player;
		
		public function Player(startX:Number, startY:Number, i:uint = 0, sx:uint = 8, sy:uint = 8):void {
			super(startX, startY, null);
			makeGraphic(sizeX, sizeY, i ? 0xffffff00 : 0xffff00ff, true);
			drag.x = RunSpeed * 8;
			maxVelocity.x = RunSpeed;
			acceleration.y = GravityAcceleratioin;
			maxVelocity.y = JumpAcceleration;
			sizeX = sx;
			sizeY = sy;
			index = i;
		}
		
		public function setPartner(p:Player):void {
			partner = p;
		}
		override public function update():void {
			acceleration.x = 0;
			if(index == 0) {
				if (FlxG.keys.LEFT) {
					facing = LEFT;
					acceleration.x = - drag.x;
				} else if (FlxG.keys.RIGHT) {
					facing = RIGHT;
					acceleration.x = drag.x;
				}
				if (FlxG.keys.UP && isTouching(FLOOR)) {
					velocity.y = - JumpAcceleration;
				}
				if (FlxG.keys.DOWN) {
					x = partner.x;
					y = partner.y;
					velocity.x += partner.velocity.x;
					velocity.y += partner.velocity.y;
					facing = velocity.x > 0 ? RIGHT : LEFT;
				}
			} else {
				if (FlxG.keys.A) {
					facing = LEFT;
					acceleration.x = - drag.x;
				} else if (FlxG.keys.D) {
					facing = RIGHT;
					acceleration.x = drag.x;
				}
				if (FlxG.keys.W && isTouching(FLOOR)) {
					velocity.y = - JumpAcceleration;
				}
				if (FlxG.keys.S) {
					x = partner.x;
					y = partner.y;
					velocity.x += partner.velocity.x;
					velocity.y += partner.velocity.y;
					facing = velocity.x > 0 ? RIGHT : LEFT;
				}
			}
			separateY(this, partner);
			super.update();
		}
	}

}