package {
	import org.flixel.*;
	
	public class StageState extends FlxState {
		private const numParticles:uint = 1000;
		[Embed(source = 'assets/tiles.png')] private var par:Class;
		private var t:FlxText;
		public function StageState():void {
		}
		override public function create():void {
			t = new FlxText(FlxG.width / 2 - 50, FlxG.height / 2 - 50, 100, "Press S to start");
			t.size = 20;
			t.alignment = "center";
			add(t);
			var emitter:FlxEmitter = new FlxEmitter(20, 20);
			emitter.makeParticles(par, 5);
			add(emitter);
			emitter.start();
		}
		override public function update():void {
			super.update();
			if (FlxG.mouse.justPressed()) {
				var emitter:FlxEmitter = new FlxEmitter(FlxG.mouse.x, FlxG.mouse.y);
				emitter.makeParticles(par, 5);
				add(emitter);
				emitter.start();
			}
			if (FlxG.keys.S) {
				FlxG.switchState(new NewPlaystate());
			}
		}
	}
}