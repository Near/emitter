package {
	import org.flixel.*;
	
	[SWF(width = "400", height = "400", backgroundColor = "#000000")];
	
	public class Emitter extends FlxGame {
		public function Emitter():void {
			super(400, 400, StageState, 1.5);
			FlxG.mouse.show();
		}
	}
}