package  
{
	import org.flixel.*;
	
	public class PlayState extends FlxState
	{
		private var player : Array;
		private var map : Map;
		public function PlayState():void {
		}
		
		override public function create():void {
			FlxG.framerate = 50;
			FlxG.flashFramerate = 50;
			player = new Array(new Player(8, 8, 0), new Player(16, 8, 1));
			player[0].setPartner(player[1]);
			player[1].setPartner(player[0]);
			add(player[0]);
			add(player[1]);
			
			map = new Map();
		}
		
		override public function update():void {
			FlxG.collide(map.getTiles(), player[0]);
			FlxG.collide(map.getTiles(), player[1]);
			super.update();
		}
	}

}